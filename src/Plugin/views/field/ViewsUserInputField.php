<?php

namespace Drupal\views_user_input_field\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ResultRow;
use Drupal\Component\Utility\Html;

/**
 * Provides Views user input field handler.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("views_user_input_field")
 */
class ViewsUserInputField extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Service request_stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->requestStack = $container->get('request_stack');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['query_string_key'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['query_string_key'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Query string key'),
      '#description' => $this->t('In case of collecting user input from an HTML form, this is the value of the "name" attribute of the form element. In the case of getting the exposed filter date, this is the "Filter identifier".'),
      '#default_value' => $this->options['query_string_key'],
    ];
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $request = $this->requestStack->getCurrentRequest();
    $queryStringKey = $this->options['query_string_key'];
    $result = $request->query->get($queryStringKey);
    $result = Html::escape($result);

    return [
      '#markup' => $result,
      '#cache' => [
        'contexts' => ['url.query_args:' . $queryStringKey],
      ],
    ];
  }

}
