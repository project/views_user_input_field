# Views User Input Field

This is module used to enable to collect user's input from HTML Form, exposed filter or webforms.
Then this data can used in calculations along with data.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/views_user_input_field).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/views_user_input_field).


## Table of contents

- Uses
- Requirements
- Installation
- Troubleshooting
- Maintainers


## Uses

- We can do the calculation with Views Simple Math Field module, so let's install it.

- To enter the amount of products by a user, we can create either HTML form or Webform (created with Webform module). The HTML form can be created in a custom block or in a view header.

- To get the amount entered by the user, let's add the "Views user input field" field provided by this module.

- The last thing we need to do is add a "Simple Math Field" and enter the formula to calculate the total price.


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Troubleshooting

If not displayed updated content, check the following:

- Clear the Drupal cache if is not displayed updated content.


## Maintainers

- Andrey Vitushkin (wombatbuddy) - [Andrey Vitushkin](https://www.drupal.org/u/wombatbuddy)
